from django.urls import path
from . import views

from django.conf.urls import url

app_name = 'blog'
urlpatterns = [
	path('', views.post_list, name = 'post_list'),
	path('popularpost', views.post_list, name = 'popularpost'),
	path('tag/<slug:tag_slug>', views.post_list, name='post_list_by_tag'),

	path('<int:question_id>/', views.detail, name = 'detail'),
	path('<int:question_id>/leave_comment/', views.leave_comment, name = 'leave_comment'),

	path('newpost/', views.newpost_page, name = 'newpost_page'),
	path('newpost/add', views.post_new, name='post_new'),

	path('createacc', views.createacc, name='createacc'),
	path('createacc/su', views.createacc_su, name='createaccsu'),

	path('change', views.change, name='change'),
	path('change/login', views.change_login, name='changelogin'),
]
