from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse

from .models import Question, Answer, LikeDislike, Profile

from taggit.models import Tag

from django.core.paginator import Paginator

from django.shortcuts import get_object_or_404

from django.utils import timezone

from django.contrib.auth.models import User

from .forms import add_post


def post_list(request, tag_slug=None):
    questions = Question.objects.order_by('-pub_date')
    tag = ""
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        questions = questions.filter(tags__in=[tag])

    posts = paginate(questions, request, 3)
    answers = {}
    for question in Question.objects.all():
    	answers[question.id] = Answer.objects.filter(question=question.id).count() 


    users = User.objects.all()
    profiles = Profile.objects.all()
	  
    return render(request, 'index.html', {'posts': posts,'tag':tag,'answers': answers, 'profiles': profiles, 'users': users})

def paginate(objects_list, request, obj_per_list = 5):
    paginator = Paginator(objects_list, obj_per_list)
    page = request.GET.get('page')
    items = paginator.get_page(page)
    return items


def detail(request, question_id):
	try:
		a = Question.objects.get(id = question_id)
	except:
		raise Http404("Статья не найдена!")

	answers_list = a.answer_set.order_by('-id')
	answers_list_count = a.answer_set.order_by('-id').count()
	users = User.objects.all()
	profiles = Profile.objects.all()

	return render(request, 'detail.html', {'question': a, 'answers_list': answers_list, 'answers_list_count': answers_list_count, 'profiles': profiles, 'users': users})



def leave_comment(request, question_id):
	try:
		a = Question.objects.get(id = question_id)
	except:
		raise Http404("Статья не найдена!")

	a.answer_set.create(author_id  = request.user, answer_text = request.POST['text'])

	return HttpResponseRedirect(reverse('blog:detail', args = (a.id,)))


def newpost_page(request):
	form = add_post()
	return render(request,'newpost.html', {'form': form})


def post_new(request):
    if request.method == "POST":
        form = add_post(request.POST)
        if form.is_valid():
        	post = form.save(commit=False)
        	post.author_id = request.user
        	post.pub_date = timezone.now()
        	post.save()
        	form.save_m2m()
        	return HttpResponseRedirect(reverse('blog:newpost_page'))
    else:
    	form = add_post()

    return render(request, 'newpost.html', {'form': form})



def createacc_su(request):
	user = User.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
	user.save()
	profile = Profile.objects.create(user = user)
	profile.save()
	mes = "Аккаунт создан!"
	return render(request,'create_acc.html', {'mes': mes})

def createacc(request):
	return render(request,'create_acc.html')


def change_login(request):
	user = User.objects.get(username = request.user)
	if( request.POST.get("username")!=''):
		user.username = request.POST.get("username")
		user.save()

	if( request.POST.get("email")!=''):
		user.email = request.POST.get("email")
		user.save()
		

	if( request.POST.get("firstname")!=''):
		user.first_name = request.POST.get("firstname")
		user.save()	
	
	uname = user.username
	uemail = user.email
	ufirstname = user.first_name

	mes = 'Данные обновлены!'
	return render(request,'change.html', {'mes': mes,'uname': uname, 'uemail': uemail, 'ufirstname': ufirstname})

def change(request):
	user = User.objects.get(username = request.user)
	uname = user.username
	uemail = user.email
	ufirstname = user.first_name
	return render(request,'change.html', {'uname': uname, 'uemail': uemail, 'ufirstname': ufirstname})



